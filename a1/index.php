<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Building</h2>
    <p>The name of the building is <?=$building->getName();?> </p>
    <p>The <?=$building->getName();?> has <?=$building->getFloors();?> floors</p>
    <p>The <?=$building->getName();?> is located at <?=$building->getAddress();?></p>
    <p>The name of the building has been changed to <?php $building->setName("Caswynn Complex");?> <?=$building->getName();?></p>

    <h2>Condominium</h2>
    <p>The name of the condominium is <?=$condominium->getName();?> </p>
    <p>The <?=$condominium->getName();?> has <?=$condominium->getFloors();?> floors</p>
    <p>The <?=$condominium->getName();?> is located at <?=$condominium->getAddress();?></p>
    <p>The name of the condominium has been changed to <?php $condominium->setName("Enzo Tower");?> <?=$condominium->getName();?></p>


</body>
</html>